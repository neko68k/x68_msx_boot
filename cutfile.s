; cutfile loader

; taken from ch30_omake source provided by E.Watanabe
; NIFTY-Serve MHF03337
; �ق���PRO68 HOTI0107
; 1996/01/27  Eisuke Watanabe


.xdef cutfile_print


; a0 = cutfile address
; a1 = tvram destination start address

cutfile_print:
	movem.l	d0-d7/a0-a6,-(sp)

	move.l	a1,a6
	lea	$80(a1),a1

	move.w	(a0)+,d3		;horizontal
	move.w	d3,d7
	lsr.w	#3,d7			;d7=width/8, number of bytes
	subq.w	#1,d7           ;d7=d7-1
@@:
	clr.b	(a6)+           ; clear row
	dbra	d7,@b

	move.w	(a0)+,d4		;vertical
	subq.w	#1,d4           ;d4=height-1
cutfile_loop:
	move.l	a1,a6           ;a6=dest

	moveq.l	#0,d0
	move.b	(a0)+,d0        ; get num_bytes
	lea	-$01(a0,d0.w),a3    ; a3=next row
	cmp.b	#1,d0
	bhi	cutfile_next

	move.w	d3,d7
	lsr.w	#3,d7			;d7=width/8
	subq.w	#1,d7           ;d7=d7-1
@@:
	move.b	-$80(a6),(a6)+
	dbra	d7,@b
	bra	cutfile_loop_end

cutfile_next:
	move.w	d3,d7
	lsr.w	#6,d7			;expected_num_bytes=width/64
	lea	$01(a0,d7.w),a2		;Uncompressed image data start address

	move.w	d3,d5
	and.b	#$07,d5
	addq.b	#2,d5
cutfile_loop2:
	move.b	(a0)+,d0
	moveq.l	#8-1,d6
cutfile_loop3:
	tst.w	d7
	bne	@f
	subq.b	#1,d5
	bmi	cutfile_loop_end
@@:
	move.b	-$80(a6),d1
	btst.l	#7,d0
	beq	@f
	move.b	(a2)+,d2
	eor.b	d2,d1       ; XOR rows
@@:
	move.b	d1,(a6)+    ; write new byte to TVRAM

	rol.b	#1,d0
	dbra	d6,cutfile_loop3
	dbra	d7,cutfile_loop2

cutfile_loop_end:
	move.l	a3,a0
	lea	$80(a1),a1
	dbra	d4,cutfile_loop

	movem.l	(sp)+,d0-d7/a0-a6
	rts
