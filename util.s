.xdef vsync_wait
.xdef vsync_wait_sub

; wait 'd7' frames
vsync_wait_sub:
	bsr	vsync_wait
	dbra	d7,vsync_wait_sub
	rts

; wait 1 frame
vsync_wait:
	btst.b	#4,$00e88001
	beq	vsync_wait
@@:
	btst.b	#4,$00e88001
	bne	@b
	rts
