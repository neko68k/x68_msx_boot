AS=HAS060
LK=gcc
OBJ=main.o util.o cutfile.o msx.o

msx.sys : msx.r
	mv msx.r msx.sys

msx.r : msx.x
	HCV /rn msx.x

msx.x : $(OBJ)
	$(LK) -omsx.x  $(OBJ) -liocs
    
clean :
	rm $(OBJ)
    
%.o : %.s
	$(AS) -u $<

