#include <iocslib.h>

void opening_exec(void);

int main(int argc, char* argv[])
{
    int oldssp = 0;
    
    // disable cursor
    _iocs_os_curof();
    
    // enter supervisor mode
    oldssp = _iocs_b_super(0);
    
    // exec the code
    opening_exec();
    
    // back to use more
    _iocs_b_super(oldssp);
    
    // cursor on
    _iocs_os_curon();
    
    // 768x512
    _iocs_crtmod(0x10);
    return 0;
}

